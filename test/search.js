const dotenv = require('dotenv').config({
    path: './test/.env',
})
const expect = require('chai').expect
const axios = require('axios')
const moment = require('moment')
const propertyClient = require('./../index.js')
const Property = require('./../classes/property.js')

it('Properties Search', function(done) {

    const client = new propertyClient(process.env.API_KEY, {
        size: 1,
        from: 0,
        status: 'Available',
    })

    client.getProperties()
    .then((response) => {
        // check variable type
        expect(response.rawRows).to.be.an('array')
        expect(response.parsedRows).to.be.an('array')
        expect(response.count).to.be.an('number')
        expect(response.total).to.be.an('number')
        expect(response.count).to.equal(1)

        // loop through raw rows
        for (const row of response.rawRows) {
            validateRawProperty(row['_source'])
        }

        // loop through parsed rows
        for (const property of response.parsedRows) {
            validateParsedProperty(property)
        }

        done()
    })
    .catch(error => {
        console.log('TESTING ERROR:', error)
    })

})

it('Property Search', function(done) {

    const client = new propertyClient(process.env.API_KEY)

    client.getProperty(process.env.WEB_ID)
    .then((response) => {
        validateParsedProperty(response.parsedRow)
        done()
    })
    .catch(error => {
        console.log('TESTING ERROR:', error)
    })
})

// internal testing functions
function validateRawProperty(property)
{
    // check keys exist in object
    expect(property).to.include.all.keys([
        'CompanyID',
        'WebID',
        'PortalOptions',
        'Brochures',
        'Features',
        'Address',
        'EPCs',
        'Fees',
        'HIPs',
        'Tenure',
        'FloorPlans',
        'Property',
        'URLs',
        'Office',
        'Contact',
        'Photos',
        'Postcode',
        'Location',
        'Videos',
        'Size',
        'Charges',
    ])

    // check variable is not undefined
    expect(property.CompanyID).to.not.be.undefined
    expect(property.WebID).to.not.be.undefined
    expect(property.PortalOptions).to.be.an('string')
    expect(property.WebID).to.be.an('number')

    // check variable is an object
    expect(property.Brochures).to.be.an('object')
    expect(property.Features).to.be.an('object')
    expect(property.Address).to.be.an('object')
    expect(property.EPCs).to.be.an('object')
    expect(property.Fees).to.be.an('object')
    expect(property.HIPs).to.be.an('object')
    expect(property.Tenure).to.be.an('object')
    expect(property.FloorPlans).to.be.an('object')
    expect(property.Property).to.be.an('object')
    expect(property.URLs).to.be.an('object')
    expect(property.Office).to.be.an('object')
    expect(property.Contact).to.be.an('object')
    expect(property.Photos).to.be.an('object')
    expect(property.Postcode).to.be.an('object')
    expect(property.Location).to.be.an('object')
    expect(property.Videos).to.be.an('object')
    expect(property.Size).to.be.an('object')
    expect(property.Charges).to.be.an('object')
}

function validateParsedProperty(property)
{
    // check variable is an instance of the 'Property' class
    expect(property).to.be.an.instanceof(Property)

    // check variable is not 'undefined'
    expect(property.title).to.not.be.undefined
    expect(property.companyId).to.not.be.undefined
    expect(property.propertyId).to.not.be.undefined
    expect(property.id).to.not.be.undefined
    expect(property.rentPeriod).to.not.be.undefined
    expect(property.webStatus).to.not.be.undefined
    expect(property.brochures).to.not.be.undefined
    expect(property.features).to.not.be.undefined
    expect(property.address).to.not.be.undefined
    expect(property.addressParts).to.not.be.undefined
    expect(property.addressString).to.not.be.undefined
    expect(property.epcImages).to.not.be.undefined
    expect(property.epcDocuments).to.not.be.undefined
    expect(property.feesDescription).to.not.be.undefined
    expect(property.feesUrl).to.not.be.undefined
    expect(property.tenure).to.not.be.undefined
    expect(property.tenureType).to.not.be.undefined
    expect(property.floorPlans).to.not.be.undefined
    expect(property.category).to.not.be.undefined
    expect(property.description).to.not.be.undefined
    expect(property.charge).to.not.be.undefined
    expect(property.featured).to.not.be.undefined
    expect(property.featuredDate).to.not.be.undefined
    expect(property.updatedDate).to.not.be.undefined
    expect(property.criteriaType).to.not.be.undefined
    expect(property.amount).to.not.be.undefined
    expect(property.availableFromDate).to.not.be.undefined
    expect(property.town).to.not.be.undefined
    expect(property.area).to.not.be.undefined
    expect(property.summaryDescription).to.not.be.undefined
    expect(property.rentPeriod).to.not.be.undefined
    expect(property.priceQualifier).to.not.be.undefined
    expect(property.propertyType).to.not.be.undefined
    expect(property.outsideSpace).to.not.be.undefined
    expect(property.parking).to.not.be.undefined
    expect(property.floors).to.not.be.undefined
    expect(property.bedrooms).to.not.be.undefined
    expect(property.bathrooms).to.not.be.undefined
    expect(property.furnished).to.not.be.undefined
    expect(property.sellingState).to.not.be.undefined
    expect(property.marketingDescription).to.not.be.undefined
    expect(property.marketingDescriptionHtml).to.not.be.undefined
    expect(property.newProperty).to.not.be.undefined
    expect(property.keywords).to.not.be.undefined
    expect(property.groundRent).to.not.be.undefined
    expect(property.newHome).to.not.be.undefined
    expect(property.insertDate).to.not.be.undefined
    expect(property.urls).to.not.be.undefined
    expect(property.office).to.not.be.undefined
    expect(property.officeId).to.not.be.undefined
    expect(property.officeName).to.not.be.undefined
    expect(property.officePhone).to.not.be.undefined
    expect(property.officeEmail).to.not.be.undefined
    expect(property.officeManager).to.not.be.undefined
    expect(property.officeUrl).to.not.be.undefined
    expect(property.contact).to.not.be.undefined
    expect(property.contactName).to.not.be.undefined
    expect(property.contactEmail).to.not.be.undefined
    expect(property.photos).to.not.be.undefined
    expect(property.photoUrl).to.not.be.undefined
    expect(property.postcode).to.not.be.undefined
    expect(property.location).to.not.be.undefined
    expect(property.longitude).to.not.be.undefined
    expect(property.latitude).to.not.be.undefined
    expect(property.videos).to.not.be.undefined

    // check ID is a number
    expect(property.id).to.be.an('number')

    // check variable is an array
    expect(property.brochures).to.be.an('array')
    expect(property.features).to.be.an('array')
    expect(property.epcImages).to.be.an('array')
    expect(property.epcDocuments).to.be.an('array')
    expect(property.photos).to.be.an('array')
    expect(property.videos).to.be.an('array')
    expect(property.addressParts).to.be.an('array')

    // check variable is an object
    expect(property.office).to.be.an('object')
    expect(property.contact).to.be.an('object')
    expect(property.location).to.be.an('object')

    // check variable is a date
    if(property.featuredDate) {
        expect(property.featuredDate).to.be.an.instanceof(moment)
    }

    if(property.updatedDate) {
        expect(property.updatedDate).to.be.an.instanceof(moment)
    }

    if(property.insertDate) {
        expect(property.insertDate).to.be.an.instanceof(moment)
    }

    if(property.availableFromDate) {
        expect(property.availableFromDate).to.be.an.instanceof(moment)
    }
}