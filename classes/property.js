const moment = require('moment')

const mediaUrl = 'https://passport.eurolink.co/api/properties/v1/media/'

module.exports = class Property {
    constructor(row) {
        this.row = row
    }

    // static methods
    static get MEDIA_URL() {
        return mediaUrl
    }

    static get FEATURES_MAX() {
        return 20
    }

    static get BROCHURES_MAX() {
        return 2
    }

    static get EPC_IMAGES_MAX() {
        return 2
    }

    static get EPC_DOCUMENTS_MAX() {
        return 2
    }

    static get FLOOR_PLANS_MAX() {
        return 5
    }

    static get URLS_MAX() {
        return 2
    }

    static get PHOTOS_MAX() {
        return 25
    }

    static get VIDEOS_MAX() {
        return 4
    }

    // getters
    get property() {
        return this.row.Property
    }

    get title() {
        return this.row.Property.ShortAddress
    }

    get companyId() {
        return this.row.CompanyID
    }

    get propertyId() {
        return this.row.Property.PropertyID
    }

    get id() {
        return this.row.WebID
    }

    get rentPeriod() {
        return this.row.Property.RentPeriod
    }

    get webStatus() {
        return this.row.Property.Status
    }

    get brochures() {
        return this.formatBrochures()
    }

    get features() {
        return this.formatFeatures()
    }

    get address() {
        return this.row.Address
    }

    get addressParts() {
        let addressParts = []

        for (const [key, value] of Object.entries(this.row.Address)) {
            if(value) {
                addressParts.push(value)
            }
        }

        return addressParts
    }

    get addressString() {
        return this.addressParts.join(', ')
    }

    get shortAddress() {
        return this.row.Property.ShortAddress
    }

    get epcImages() {
        return this.formatEpcImages()
    }

    get epcDocuments() {
        return this.formatEpcDocuments()
    }

    get feesDescription() {
        return this.row.Fees.Description
    }

    get feesUrl() {
        return this.row.Fees.Link
    }

    get tenure() {
        return this.row.Tenure.Tenure
    }

    get tenureType() {
        return this.row.Tenure.TenureType
    }

    get floorPlans() {
        return this.formatfloorPlans()
    }

    get category() {
        return this.row.Property.Category
    }

    get description() {
        return this.row.Property.Description
    }

    get charge() {
        return this.row.Property.Charge
    }

    get featured() {
        return (this.row.Property.Featured ? true : false)
    }

    get featuredDate() {
        return (this.row.Property.Featured ? this.parseDate(this.row.Property.FeaturedDate) : null)
    }

    get updatedDate() {
        return this.parseDate(this.row.Property.UpdatedDate)
    }

    get criteriaType() {
        return this.row.Property.CriteriaType
    }

    get amount() {
        return this.row.Property.Amount
    }

    get amountAsInteger() {
        return this.row.Property.AmountInt
    }

    get availableFromDate() {
        return this.parseDate(this.row.Property.AvailableFromDate)
    }

    get town() {
        return this.row.Property.Town
    }

    get area() {
        return this.row.Property.Area
    }

    get summaryDescription() {
        return this.row.Property.SummaryDescription
    }

    get rentPeriod() {
        return this.row.Property.RentPeriod
    }

    get priceQualifier() {
        return this.row.Property.PriceStatus
    }

    get propertyType() {
        return this.row.Property.PropertyType
    }

    get outsideSpace() {
        return this.row.Property.OutsideSpace
    }

    get parking() {
        return this.row.Property.Parking
    }

    get floors() {
        return this.row.Property.Floors
    }

    get bedrooms() {
        return this.row.Property.Bedrooms
    }

    get bathrooms() {
        return this.row.Property.Bathrooms
    }

    get furnished() {
        return this.row.Property.Furnished
    }

    get sellingState() {
        return this.row.Property.SellingState
    }

    get marketingDescription() {
        return this.row.Property.MarketingDescription
    }

    get marketingDescriptionHtml() {
        return this.row.Property.MarketingDescriptionHTML
    }

    get newProperty() {
        return this.row.Property.NewProperty
    }

    get keywords() {
        return this.row.Property.Keywords
    }

    get groundRent() {
        return this.row.Property.GroundRent
    }

    get newHome() {
        return this.row.Property.NewHome
    }

    get insertDate() {
        return this.parseDate(this.row.Property.InsertDate)
    }

    get councilTaxBand() {
        return this.row.Property.CouncilTaxBand
    }

    get isSharedOwnership() {
        return this.row.Property.SharedOwnership
    }

    get sharedOwnershipPercentage() {
        return this.row.Property.SharedOwnershipPercentage
    }

    get sharedOwnershipRentFrequency() {
        return this.row.Property.SharedOwnershipRentFrequency
    }

    get sharedOwnershipRentAmount() {
        return this.row.Property.SharedOwnershipRentAmount
    }

    get depositAmount() {
        return this.row.Property.DepositAmount
    }

    get urls() {
        return this.formatUrls()
    }

    get office() {
        return this.row.Office
    }

    get officeId() {
        return this.row.Office.ID
    }

    get officeName() {
        return this.row.Office.Name
    }

    get officePhone() {
        return this.row.Office.Phone
    }

    get officeEmail() {
        return this.row.Office.Email
    }

    get officeManager() {
        return this.row.Office.Manager
    }

    get officeUrl() {
        return this.row.Office.Website
    }

    get contact() {
        return this.row.Contact
    }

    get contactName() {
        return this.row.Contact.Name
    }

    get contactEmail() {
        return this.row.Contact.Email
    }

    get photos() {
        return this.formatPhotos()
    }

    get photoUrl() {
        return this.photos[0] || null
    }

    get postcode() {
        return this.row.Postcode.PostcodeFull
    }

    get location() {
        return this.row.Location
    }

    get longitude() {
        return this.row.Location.Longitude
    }

    get latitude() {
        return this.row.Location.Latitude
    }

    get videos() {
        return this.formatVideos()
    }

    // methods
    formatBrochures() {
        let brochures = []

        for (let i = 1; i <= Property.BROCHURES_MAX; i++) {
            const document = this.row.Brochures[`Document${i}`]

            if(document) {
                brochures.push({
                    url: Property.MEDIA_URL + document,
                    description: this.row.Brochures[`Description${i}`],
                })
            }
        }

        return brochures
    }

    formatFeatures() {
        let features = []

        for (let i = 1; i <= Property.FEATURES_MAX; i++) {
            const feature = this.row.Features[`Feature${i}`]

            if(feature) {
                features.push(feature)
            }
        }

        return features
    }

    formatEpcImages() {
        let epcImages = []

        for (let i = 1; i <= Property.EPC_IMAGES_MAX; i++) {
            const image = this.row.EPCs[`Image${i}`]

            if(image) {
                epcImages.push({
                    url: Property.MEDIA_URL + image,
                })
            }
        }

        return epcImages
    }

    formatEpcDocuments() {
        let epcDocuments = []

        for (let i = 1; i <= Property.EPC_DOCUMENTS_MAX; i++) {
            const document = this.row.EPCs[`Document${i}`]

            if(document) {
                epcDocuments.push({
                    url: Property.MEDIA_URL + document,
                })
            }
        }

        return epcDocuments
    }

    formatfloorPlans() {
        let floorPlans = []

        for (let i = 1; i <= Property.FLOOR_PLANS_MAX; i++) {
            const plan = this.row.FloorPlans[`Plan${i}`]

            if(plan) {
                floorPlans.push({
                    url: Property.MEDIA_URL + plan,
                })
            }
        }

        return floorPlans
    }

    formatUrls() {
        let urls = []

        for (let i = 1; i <= Property.URLS_MAX; i++) {
            const url = this.row.URLs[`URL${i}`]

            if(url) {
                urls.push(url)
            }
        }

        return urls
    }

    formatPhotos() {
        let photos = []

        for (let i = 1; i <= Property.PHOTOS_MAX; i++) {
            const photo = this.row.Photos[`Photo${i}`]

            if(photo) {
                photos.push({
                    url: Property.MEDIA_URL + photo,
                    description: this.row.Photos[`Description${i}`],
                })
            }
        }

        return photos
    }

    formatVideos() {
        let videos = []

        for (let i = 1; i <= Property.VIDEOS_MAX; i++) {
            const video = this.row.Videos[`Video${i}`]

            if(video) {
                videos.push({
                    url: video,
                    description: this.row.Videos[`Description${i}`],
                })
            }
        }

        return videos
    }

    parseDate(dateString) {
        return (dateString ? moment(dateString) : null)
    }
}