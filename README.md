## Property API JavaScript Client

A simple NodeJS package to make HTTP requests to [Eurolink's](https://www.eurolink.co/) 'Property API'. This library is restricted for usage by approved clients who have been allowed access to the Property API.

Created and maintained by [Eurolink Technology Limited](https://www.eurolink.co/).

Please direct any support enquiries to the [Eurolink support desk](https://hub.eurolink.co/) or email support@eurolink.co.

API documentation is available online: [https://property-api.readme.io/](https://property-api.readme.io/).

#### Install

`npm install property-api-javascript-client --save`

#### Usage (with Promises)

```
/* search properties */
const propertyApiClient = require('property-api-javascript-client')

const client = new propertyApiClient('TOKEN_GOES_HERE', {
    // add extra query parameters here
    status: 'Available',
})

client.getProperties()
.then((response) => {
    console.log(response.total, response.count, response.rawRows, response.parsedRows)
})
```

```
/* get a property by ID */
const propertyApiClient = require('property-api-javascript-client')

const client = new propertyApiClient('TOKEN_GOES_HERE')

client.getProperty(12345)
.then((response) => {
    console.log(response.rawRow, response.parsedRow)
})
```

#### Usage (with async/await)
```
/* search properties */
const propertyApiClient = require('property-api-javascript-client')

// must be inside an async function
(async() => {
    const client = new propertyApiClient('TOKEN_GOES_HERE', {
        // add extra query parameters here
        status: 'Available',
    })

    const response = await client.getProperties()

    console.log(response.total, response.count, response.rawRows, response.parsedRows)
})()
```

```
/* get a property by ID */
const propertyApiClient = require('property-api-javascript-client')

// must be inside an async function
(async() => {
    const client = new propertyApiClient('TOKEN_GOES_HERE')

    const response = await client.getProperty(12345)

    console.log(response.rawRow, response.parsedRow)
})()
```

#### Methods
Once you have a valid response, you use the following [getters](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/get) on a parsed row:

- row.property
- row.title
- row.companyId
- row.propertyId
- row.id
- row.rentPeriod
- row.webStatus
- row.brochures
- row.features
- row.address
- row.addressParts
- row.addressString
- row.shortAddress
- row.epcImages
- row.epcDocuments
- row.feesDescription
- row.feesUrl
- row.tenure
- row.tenureType
- row.floorPlans
- row.category
- row.description
- row.charge
- row.featured
- row.featuredDate
- row.updatedDate
- row.criteriaType
- row.amount
- row.availableFromDate
- row.town
- row.area
- row.summaryDescription
- row.rentPeriod
- row.priceQualifier
- row.propertyType
- row.outsideSpace
- row.parking
- row.floors
- row.bedrooms
- row.bathrooms
- row.furnished
- row.sellingState
- row.marketingDescription
- row.marketingDescriptionHtml
- row.newProperty
- row.keywords
- row.groundRent
- row.newHome
- row.insertDate
- row.councilTaxBand
- row.isSharedOwnership
- row.sharedOwnershipPercentage
- row.sharedOwnershipRentFrequency
- row.sharedOwnershipRentAmount
- row.depositAmount
- row.urls
- row.office
- row.officeId
- row.officeName
- row.officePhone
- row.officeEmail
- row.officeManager
- row.officeUrl
- row.contact
- row.contactName
- row.contactEmail
- row.photos
- row.photoUrl
- row.postcode
- row.location
- row.longitude
- row.latitude
- row.videos

```
// for example...
for (const row of response.parsedRows) {
    console.log(row.title)
}
```

#### Testing
Copy `test/env.example` to `test/.env`. Add your own API key and a related property Web ID. Then run the following command:

```
npm test
```

#### Publish on NPM
Update the `version` in `package.json`. Then run the following command:

```
npm publish
```

You'll need to login first, using these [credentials](https://eurolink.1password.com/vaults/all/allitems/hgi4o6xbrywc7maw7k23lllfga).
```
npm login
```