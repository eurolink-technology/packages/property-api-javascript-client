const axios = require('axios')
const Property = require('./classes/property.js')

const passportUrl = 'https://passport.eurolink.co/api/properties/v1'

module.exports = class Client {
    constructor(token, params) {
        this.token = token
        this.params = params
        this.response = {}
    }

    // static methods
    static get PASSPORT_URL() {
        return passportUrl
    }

    // methods
    async getProperties() {
        return axios({
            method: 'get',
            url: Client.PASSPORT_URL,
            headers: {
                'Authorization': `Bearer ${this.token}`,
            },
            params: this.params,
        })
        .then((response) => {
            this.response.total = response.data.Total
            this.response.count = response.data.Count
            this.response.rawRows = response.data.Data
            this.response.parsedRows = []

            if(this.response.rawRows && this.response.rawRows.length) {
                for (const row of this.response.rawRows) {
                    this.response.parsedRows.push(new Property(row['_source']))
                }
            }

            return this.response
        })
    }

    async getProperty(id) {
        return axios({
            method: 'get',
            url: `${Client.PASSPORT_URL}/id/${id}`,
            headers: {
                'Authorization': `Bearer ${this.token}`,
            },
        })
        .then((response) => {
            this.response.total = response.data.Total
            this.response.count = response.data.Count
            this.response.rawRow = response.data.Data[0]

            this.response.parsedRow = new Property(this.response.rawRow['_source'])

            return this.response
        })
    }
}